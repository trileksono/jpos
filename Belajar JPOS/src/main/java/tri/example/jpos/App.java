package tri.example.jpos;

import org.jpos.q2.Q2;

public class App {

  public static void main(String[] args) {
    try {
      new Q2().start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
