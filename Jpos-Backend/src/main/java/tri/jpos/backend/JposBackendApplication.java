package tri.jpos.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@SpringBootApplication
@EntityScan(basePackageClasses = {JposBackendApplication.class, Jsr310JpaConverters.class})
public class JposBackendApplication {

  public static void main(String[] args) {
    SpringApplication.run(JposBackendApplication.class, args);
  }
}
