package tri.jpos.backend.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by tri on 12/21/16.
 */
@Entity
@Table(name = "mutasi")
public class Mutasi {
  @Id @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  @NotNull
  @ManyToOne
  @JoinColumn(name = "id_rekening", nullable = false)
  private Rekening rekening;

  @NotNull
  @Column(name = "waktu_transaksi", nullable = false)
  private LocalDateTime waktuTransaksi = LocalDateTime.now();

  @NotNull
  @Column(nullable = false)
  private BigDecimal nilai;

  @NotNull @NotEmpty
  @Size(min = 3, max = 255)
  private String keterangan;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Rekening getRekening() {
    return rekening;
  }

  public void setRekening(Rekening rekening) {
    this.rekening = rekening;
  }

  public LocalDateTime getWaktuTransaksi() {
    return waktuTransaksi;
  }

  public void setWaktuTransaksi(LocalDateTime waktuTransaksi) {
    this.waktuTransaksi = waktuTransaksi;
  }

  public BigDecimal getNilai() {
    return nilai;
  }

  public void setNilai(BigDecimal nilai) {
    this.nilai = nilai;
  }

  public String getKeterangan() {
    return keterangan;
  }

  public void setKeterangan(String keterangan) {
    this.keterangan = keterangan;
  }


}
