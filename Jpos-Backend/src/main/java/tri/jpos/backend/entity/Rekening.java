package tri.jpos.backend.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Created by tri on 12/21/16.
 */
@Entity
@Table(name = "rekening")
public class Rekening {
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  @NotNull
  @NotEmpty
  @Size(min = 3, max = 10)
  @Column(unique = true, nullable = false)
  private String nomor;

  @NotNull @NotEmpty @Size(min = 3, max = 255)
  @Column(nullable = false)
  private String nama;

  @NotNull
  private BigDecimal saldo = BigDecimal.ZERO;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNomor() {
    return nomor;
  }

  public void setNomor(String nomor) {
    this.nomor = nomor;
  }

  public String getNama() {
    return nama;
  }

  public void setNama(String nama) {
    this.nama = nama;
  }

  public BigDecimal getSaldo() {
    return saldo;
  }

  public void setSaldo(BigDecimal saldo) {
    this.saldo = saldo;
  }


}