package tri.jpos.backend.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import tri.jpos.backend.entity.Rekening;

/**
 * Created by tri on 12/21/16.
 */
public interface RekeningDao extends PagingAndSortingRepository<Rekening, String> {
  public Rekening findByNomor(String nomor);
}