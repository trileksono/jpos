package tri.jpos.backend.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import tri.jpos.backend.entity.Mutasi;
import tri.jpos.backend.entity.Rekening;

/**
 * Created by tri on 12/21/16.
 */
public interface MutasiDao extends PagingAndSortingRepository<Mutasi, String> {
  public Page<Mutasi> findByRekening(Rekening r, Pageable page);
}